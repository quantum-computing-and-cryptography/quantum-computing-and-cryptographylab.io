function updateList() {
  container = document.querySelector(".container");
  listItems = Array.from(document.querySelectorAll(".list-item")); // Array of elements
  sortables = listItems.map(Sortable); // Array of sortables
  total     = sortables.length;
  TweenLite.to(container, 0.5, { autoAlpha: 1 });
	changeIndex(sortables[0], 0);
}

function cos(degrees) {
  return Math.cos(degrees*Math.PI/180);
}

function Polaroid(rotation) {
  let p = Object.create(_Polaroid);
  p.rotation = rotation;
  return p;
}

function Photon(polarization) {
  let p = Object.create(_Photon);
  p.polarization = polarization;
  return p;
}

const _Polaroid = {
  rotation: 0 // angle in degrees
}

const _Photon = {
  polarization: 0 // angle in degrees
}

var Polaroids = [Polaroid(0)];

// array of polaroid objects -> fractional value
function shine(polaroids) {
  let ratio = 0.5; // half of randomly polarized light will get through first polaroid
  let photon = Photon(polaroids[0].rotation);
  
  for (var polaroid of polaroids) {
    ratio *= cos(Math.abs(photon.polarization - polaroid.rotation)) ** 2;
    photon.polarization = polaroid.rotation;
  }

  return ratio;
}

function partialShine(depth) {
  return +shine(Polaroids.slice(0,depth)).toFixed(5);
}

function drawCircle() {
  const light_ratio = shine(Polaroids) * (document.getElementById('doubleLux').checked ? 2 : 1);
  const color_value = 255*light_ratio;
  document.getElementById("polaroid").style.backgroundColor = `RGBA(${color_value},${color_value}, ${color_value}, 1)`;
}

// ---------------------- //

var ItemCount = 1;

function addPolaroid() {
  ItemCount++;

  let rotation = Polaroids[Polaroids.length-1].rotation;
  Polaroids.push(Polaroid(rotation));

  var polaroid = document.createElement('div');
  polaroid.innerHTML = `
    <div class="list-item" oncontextmenu="javascript:deletePolaroid(this);return false;">
      <div class="item-content">
        <span class="order">${ItemCount}</span>
        <input size=2 value="${rotation}">
        <span class="lux">${partialShine(ItemCount)}</span>
      </div>
    </div>
  `;
  document.getElementById("polaroids").appendChild(polaroid);

  updateList()
}

function deletePolaroid(polaroid) {
	if (Polaroids.length == 1) { return }

  const i = polaroid.querySelector('.order').innerHTML-1;
	console.log(i);
	console.log(Polaroids)
  Polaroids.splice(i, 1); // remove from Polaroids list
	console.log(Polaroids)
  polaroid.remove();

  updateList()
}

function updatePolaroids() {
  var polaroidElms = document.querySelectorAll(".item-content");
  for (var i=0; i<polaroidElms.length; i++) {
    Polaroids[i].rotation = +polaroidElms[i].getElementsByTagName("input")[0].value;
    polaroidElms[i].querySelector(".lux").innerHTML = partialShine(i+1);
  }
  drawCircle()
}
window.setInterval(updatePolaroids,100);
// var polaroids = document.querySelector("input");
// var observer = new MutationObserver(function(mutations) {
//   mutations.forEach(function(mutation) {
//     updatePolaroids();
//   });    
// });
// observer.observe(polaroids, {characterData: true, subtree: true});
