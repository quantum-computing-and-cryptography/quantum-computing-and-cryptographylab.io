# Polaroids

A polarizing filters simulator

![](demo.png)

Web demo simulating light shown through polarizing filters at customizable angles.  
Live @ [https://quantum-computing-and-cryptography.gitlab.io/](https://quantum-computing-and-cryptography.gitlab.io/)

The central circle shows the intensity of light that would be expected to pass through the filters.  
Filters can be added, removed, and rearranged.  
The angle of each filter is customizable.

The "Double Lux" option doubles the amount of light making it through the filters for better visibility.  
This can be seen as starting with light polarized at the same angle as the initial filter instead of at random angles.

See the [wikipedia page on photon polarization](https://en.wikipedia.org/wiki/Photon_polarization) for more details.
